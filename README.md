# Godot 4 voxel test

an adaptation of Cavegame ([YouTube video](https://youtu.be/UMpv5kZ9-rE))
that i made to test some of Godot 4's 3D graphics features — especially SDFGI

this version is made with Godot ``v4.0.alpha13.official [59dcddc45]``

⚠️ this is a development version of 'Godot 4 voxel test'
that overhauls world generation,
so the screenshots and video demo represent release ``0.1`` and not this version

[▶️ Watch video demo](readme-assets/video-demo.mp4)

![](readme-assets/screenshots/0.png)
![](readme-assets/screenshots/1.png)
![](readme-assets/screenshots/2.png)
![](readme-assets/screenshots/3.png)

⚠️ because the world generation was very inefficient in release ``0.1``,
i fixed the world size to 32×64×32 voxels
and the viewport resolution to 480p
so that the world would generate and the game would run fast.
this no longer applies to the current development version, but since ``0.1`` is still the newest release, this is still good to know

## README table of contents

- controls
- known issues
  - in version 0.1
- LICENSES
  - scripts
  - textures
  - README assets

## controls

```
_   [W]
[A] [S] [D]
```
(physical keys)

walk

```
[     Space     ]
```
(physical key)

jump

(you have infinite air jumps)

```
Mouse Button 1
```
place block

⚠️ in this devlopment version, it's no longer possible to place blocks

```
Mouse Button 2
```
destroy block

⚠️ in this devlopment version, it's no longer possible to destroy blocks

## known issues

### in version 0.1

- you can place a block on the exact spot where you stand,
  causing you to get stuck in the block
- the world generation is very inefficient, causing the game to
  - run poorly
  - hang up during startup if the world size is set too large
- Grass blocks get generated as Stone blocks instead
- if you get out of range before placing or destroying a block,
  you place or destroy the block in the position when you were last in-range
  instead of not at all

## LICENSES

### scripts

the scripts are original scripts for this game, made by Sosasees and licensed under [MIT license](https://codeberg.org/Sosasees/mit-license/raw/branch/2022/LICENSE).
they have this boilerplate comment at the top

```
# Copyright (c) 2022 Sosasees
# this script is MIT-licensed:
# 	https://codeberg.org/Sosasees/mit-license/raw/branch/2022/LICENSE
# source:
# 	https://codeberg.org/Sosasees/godot-4-voxel-test
```

### textures

the textures are original textures for this game, made by Sosasees.
they are marked as Public Domain with [CC0 1.0 Universal](http://creativecommons.org/publicdomain/zero/1.0/)

Texture atlas, found under [``blocks.png``](blocks.png)
Stone texture, found under [``block.png``](block.png)

### README assets

the README assets are made by Sosasees.
they are licensed under [Attribution-NonCommercial 4.0 International](http://creativecommons.org/licenses/by-nc/4.0/)

- Screenshots, found under [``readme-assets/screenshots/``](readme-assets/screenshots/)
- Video Demo, found under [``readme-assets/video-demo.mp4``](readme-assets/video-demo.mp4)
